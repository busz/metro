using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SitManager : MonoBehaviour, IEnumerable<Sit> {
    private Sit[] _sits;

    public IEnumerator<Sit> GetEnumerator() {
        foreach (Sit sit in _sits) {
            yield return sit;
        };
    }

    IEnumerator IEnumerable.GetEnumerator() {
        return this.GetEnumerator();
    }

    void Start() {
        _sits = gameObject.GetComponentsInChildren<Sit>();
    }

}
