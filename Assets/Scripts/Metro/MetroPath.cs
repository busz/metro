using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetroPath : MonoBehaviour, IEnumerable<MetroStop> {

    [SerializeField]
    private MetroStop[] _path;

    bool _reversed;

    void Start() {
        if (!IsValid()) {
            Debug.Log(name + " Invalid Path");
        }
    }

    public void SetPath(MetroStop[] stops) {
        _path = stops;
    }

    public int GetLength() {
        return _path.Length;
    }


    public void Reverse() {
        _reversed = !_reversed;
    }

    public bool IsValid() {
        for (int i = 0; i + 1 < _path.Length; ++i) {
            MetroStop current = _reversed ? _path[_path.Length - i - 1] : _path[i];
            MetroStop nextStop = _reversed ? _path[_path.Length - i - 2] : _path[i + 1];
            if (!current.HasConnectionTo(nextStop)) {
                return false;
            }
        }
        return true;
    }

    public IEnumerator<MetroStop> GetEnumerator() {
        return _reversed ? GetBackwardEnumerator() : GetForwardEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator() {
        return this.GetEnumerator();
    }

    private IEnumerator<MetroStop> GetForwardEnumerator() {
        for (int i = 0; i < _path.Length; ++i) {
            yield return _path[i];
        }
    }

    private IEnumerator<MetroStop> GetBackwardEnumerator() {
        for (int i = _path.Length - 1; i >= 0; --i) {
            yield return _path[i];
        }
    }

}
