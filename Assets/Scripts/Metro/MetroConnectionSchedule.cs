using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MetroConnectionSchedule : MonoBehaviour {

    [SerializeField]
    private MetroStop[] _stops;

    public MetroStop[] GetPath(MetroStop from, MetroStop to) {
        Dictionary<MetroStop, bool> visitedMap = new Dictionary<MetroStop, bool>();
        Dictionary<MetroStop, MetroStop> predecessors = new Dictionary<MetroStop, MetroStop>();

        foreach (MetroStop stop in _stops) {
            visitedMap.Add(stop, false);
            predecessors.Add(stop, null);
        }
        visitedMap[from] = true;

        Queue<MetroStop> queue = new Queue<MetroStop>();
        queue.Enqueue(from);

        while (queue.Count > 0) {
            MetroStop currentStop = queue.Dequeue();
            if (currentStop == to) {
                return GeneratePath(predecessors, currentStop);
            }
            foreach (MetroStop adjacentStop in currentStop.GetConnections()) {         
                if (!visitedMap[adjacentStop]) {
                    visitedMap[adjacentStop] = true;
                    predecessors[adjacentStop] = currentStop;
                    queue.Enqueue(adjacentStop);
                }
            }
        }
        return null;
    }

    private MetroStop[] GeneratePath(Dictionary<MetroStop, MetroStop> predecessors, MetroStop currentStop) {
        List<MetroStop> path = new List<MetroStop>();
        path.Add(currentStop);
        MetroStop predecessor = predecessors[currentStop];
        while (predecessor != null) {
            path.Add(predecessor);
            predecessor = predecessors[predecessor];
        }
        path.Reverse();
        return path.ToArray();
    }

    public MetroStop GetRandomStop() {
        return _stops[Random.Range(0, _stops.Length)];
    }
}
