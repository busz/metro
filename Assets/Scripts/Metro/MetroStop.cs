using UnityEngine;
using System.Linq;

public delegate void MetroArrivedDelegete(Metro metro);

public class MetroStop : MonoBehaviour {
    [SerializeField]
    private MetroStop[] _connections;

    [SerializeField]
    private string _name;

    [SerializeField]
    private bool _drawConnections = true;

    private Metro _metro;
    private StopSquare _stopSquare;
    public MetroArrivedDelegete _metroArrived;

    private void Start() {
        _stopSquare = GetComponent<StopSquare>();
    }

    public bool isOccupied() {
        return _metro != null;
    }

    public void Occupy(Metro metro) {
        _metro = metro;
        _metroArrived?.Invoke(metro);
    }

    public void Release() {
        _metro = null;
    }

    public MetroStop[] GetConnections() {
        return _connections;
    }

    public bool HasConnectionTo(MetroStop metroStop) {
        return _connections.Contains(metroStop);
    }

    public string Name { get => _name; set => _name = value; }

    public void Enter(Passenger passenger) {
        _stopSquare.PutOnSquare(passenger.transform);
    }

    private void OnDrawGizmos() {
        if (_drawConnections) {
            foreach (var connection in _connections) {
                Gizmos.DrawLine(transform.position, connection.transform.position);
                Gizmos.DrawSphere(Vector3.Lerp(transform.position, connection.transform.position, 0.9f), 0.1f);
            }
        }
    }

}
