using System.Collections.Generic;
using UnityEngine;

public delegate void NextStopReached(MetroStop metroStop);

[RequireComponent(typeof(MetroPath))]
public class MetroPathFollower : MonoBehaviour {

    public NextStopReached OnNextStopReachedEvent;

    [SerializeField]
    private float _speed = 10.0f;

    private MetroPath _path;
    private IEnumerator<MetroStop> _pathEnumerator;
    private MetroStop _currentStop;
    private MetroStop _nextStop;


    private bool _isMoving = false;
    private float _moveProgress = 0.0f;
    private bool _isPathValid;
    private float _occupiedCheckMoveProgressThreshold = 0.5f;
    private float _distanceCurrentNext;

    void Start() {
        _path = GetComponent<MetroPath>();
        Init();
    }

    void Update() {
        if (ShouldMove()) {
            Move();
            if (WasNextStopReached()) {
                OnNextStopReached();
            }
        }
    }

    private bool ShouldMove() {
        return _isMoving && _nextStop != null && _isPathValid && !(ShouldCheckIfNextStopOccupied() && checkNextStopOccupied());
    }

    private bool ShouldCheckIfNextStopOccupied() {
        return _moveProgress > _occupiedCheckMoveProgressThreshold;
    }

    private bool checkNextStopOccupied() {
        return _nextStop.isOccupied();
    }

    public void StartMovement() {
        _isMoving = true;
    }

    public void StopMovement() {
        _isMoving = false;
    }

    private void Move() {
        _moveProgress += Time.deltaTime * _speed / _distanceCurrentNext;
        transform.position = Vector3.Lerp(_currentStop.transform.position, _nextStop.transform.position, _moveProgress);
    }

    public MetroStop GetNextStop() {
        return _nextStop;
    }

    private bool WasNextStopReached() {
        return _moveProgress >= 1;
    }

    private void OnNextStopReached() {
        IncrementStops();
        if (_nextStop == null) {
            _path.Reverse();
            Init();
        } else {
            CalculateCurrentNextDisctance();
            RotateTowardsNextStop();
        }
        OnNextStopReachedEvent.Invoke(_currentStop);
    }

    private void RotateTowardsNextStop() {
        Vector3 direction = (_nextStop.transform.position - _currentStop.transform.position).normalized;
        gameObject.transform.rotation = Quaternion.LookRotation(direction, Vector3.right) * Quaternion.FromToRotation(Vector3.right, Vector3.forward);
    }

    private void IncrementStops() {
        _moveProgress = 0;
        _currentStop = _nextStop;
        _nextStop = _pathEnumerator.MoveNext() ? _pathEnumerator.Current : null;
    }

    private void Init() {
        if (!IsValidatePath()) {
            Debug.Log(name + " not valid path");
            return;
        }

        if (_path.GetLength() > 0) {
            InitPathEnumerator();
            InitStops();
            transform.position = _currentStop.transform.position;
            if (_nextStop != null) {
                CalculateCurrentNextDisctance();
                RotateTowardsNextStop();
            }
        }
    }

    private bool IsValidatePath() {
        _isPathValid = _path.IsValid();
        return _isPathValid;
    }

    private void InitPathEnumerator() {
        _pathEnumerator = _path.GetEnumerator();
        _pathEnumerator.MoveNext();
    }

    private void InitStops() {
        _currentStop = _pathEnumerator.Current;
        _nextStop = _pathEnumerator.MoveNext() ? _pathEnumerator.Current : null;
    }

    private void CalculateCurrentNextDisctance() {
        _distanceCurrentNext = Vector3.Magnitude(_currentStop.transform.position - _nextStop.transform.position);
    }

}
