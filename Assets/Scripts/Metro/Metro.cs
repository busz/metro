using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MetroPathFollower))]
public class Metro : MonoBehaviour {

    [SerializeField]
    private float _waitingOnStopSeconds;

    private MetroStop _currentStop;
    private Queue<Passenger> registeredToEnter = new Queue<Passenger>();
    private Queue<Passenger> registeredToGetOff = new Queue<Passenger>();

    public MetroPathFollower MetroPathFollower { get; private set; }
    public SitManager SitManager { get; private set; }
    void Start() {
        MetroPathFollower = GetComponent<MetroPathFollower>();
        MetroPathFollower.OnNextStopReachedEvent += OnNextStopReached;
        MetroPathFollower.StartMovement();
        SitManager = GetComponentInChildren<SitManager>();
    }

    public MetroStop GetNextStop() {
        return MetroPathFollower.GetNextStop();
    }

    private void OnNextStopReached(MetroStop metroStop) {
        MetroPathFollower.StopMovement();
        _currentStop = metroStop;
        _currentStop.Occupy(this);
        StartCoroutine(ExchangePassengers());
        StartCoroutine(LeaveCoroutine());
    }

    private IEnumerator ExchangePassengers() {
        yield return new WaitForSeconds(0.2f);
        while (registeredToGetOff.Count > 0)
            registeredToGetOff.Dequeue().GetOff();
        while (registeredToEnter.Count > 0)
            registeredToEnter.Dequeue().AttemptEnter(this);
    }

    private IEnumerator LeaveCoroutine() {
        yield return new WaitForSeconds(_waitingOnStopSeconds);
        LeaveStop();
    }

    private void LeaveStop() {
        MetroPathFollower.StartMovement();
        _currentStop.Release();
        _currentStop = null;
    }

    public void RegisterToEnter(Passenger passenger) {
        registeredToEnter.Enqueue(passenger);
    }

    public void RegisterToGetOff(Passenger passenger) {
        registeredToGetOff.Enqueue(passenger);
    }
}
