using UnityEngine;

public class Sit : MonoBehaviour {

    private Passenger _passenger;

    [SerializeField]
    private SitDedicationType _dedicatedFor;

    [SerializeField]
    private bool _isNearWindow;

    public bool IsFree() {
        return _passenger == null;
    }

    public bool IsNearWindow() {
        return _isNearWindow;
    }

    public void SetPassenger(Passenger passenger) {
        _passenger = passenger;
    }

    public SitDedicationType GetDedicatedFor() {
        return _dedicatedFor;
    }

    public enum SitDedicationType {
        EVERYONE, DISABLED
    }
}
