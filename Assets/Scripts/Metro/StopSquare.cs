using UnityEngine;

public class StopSquare : MonoBehaviour {

    [SerializeField]
    private Transform _squareCenter;

    [SerializeField]
    private Vector2 _size;

    public void PutOnSquare(Transform transform) {
        transform.position = RandomPointInSquare();
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireCube(_squareCenter.position, new Vector3(_size.x, _size.y));
    }

    private Vector3 RandomPointInSquare() {
        float halfSizeX = _size.x / 2;
        float halfSizeY = _size.y / 2;
        Vector3 randomPointInSquareLocal = new Vector3(Random.Range(-halfSizeX, halfSizeX), Random.Range(-halfSizeY, halfSizeY), 0);
        return _squareCenter.position + _squareCenter.transform.rotation * randomPointInSquareLocal;
    }

}
