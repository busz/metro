using UnityEngine;
using UnityEngine.UI;

public class Labeler : MonoBehaviour
{

    [SerializeField]
    private string _labelText;

    [SerializeField]
    private Vector3 _offset;

    [SerializeField]
    private GameObject _labelPrefab;
    
    [SerializeField]
    private Canvas _canvas;
    
    private RectTransform _canvasRectTransform;
    private GameObject _labelGO;
    private Text _text;


    private void Start() {
        if (_canvas != null) {
            CreateLabel();
            _canvasRectTransform = _canvas.GetComponent<RectTransform>();
        } else {
            Debug.Log("No canvas found");
        }
    }

    private void CreateLabel() {
        _labelGO = Instantiate(_labelPrefab);
        _labelGO.transform.SetParent(_canvas.transform);
        _text = _labelGO.GetComponent<Text>();
        _text.text = _labelText;
    }

    private void Update() {
        Vector3 worldLabelPostion = gameObject.transform.position + _offset;
        Vector3 screenLabelPostion = Camera.main.WorldToScreenPoint(worldLabelPostion);
        Vector3 canvasLabelPosition;
        RectTransformUtility.ScreenPointToWorldPointInRectangle(_canvasRectTransform, screenLabelPostion, null, out canvasLabelPosition);
        _labelGO.transform.position = canvasLabelPosition;
    }
}
