using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "CompositeLookForSitStrategy", menuName = "LookForSitStrategy/Composite", order = 1)]
public class CompositeLookForSitStrategy : LookForSitStrategy {
    [SerializeField]
    private LookForSitStrategy[] _strategies;
    public override Sit[] LookForSit(SitManager sitManager) {
        Debug.Log(this.GetType().Name);
        Sit[][] resultsForStrategies = GetLookForSitResultForAllStratgies(sitManager);
        return IntersectResults(resultsForStrategies);
    }

    private Sit[][] GetLookForSitResultForAllStratgies(SitManager sitManager) {
        Sit[][] resultsForStrategies = new Sit[_strategies.Length][];
        for (int i = 0; i < _strategies.Length; ++i) {
            resultsForStrategies[i] = _strategies[i].LookForSit(sitManager);
        }
        return resultsForStrategies;
    }

    private Sit[] IntersectResults(Sit[][] resultsForStrategies) {
        Sit[] sits = resultsForStrategies[0];
        return sits.Where(sit => EachContains(resultsForStrategies, sit)).ToArray();
    }

    private bool EachContains(Sit[][] sits2d, Sit sit) {
        foreach (Sit[] sits in sits2d) {
            if (!sits.Contains(sit))
                return false;
        }
        return true;
    }
}
