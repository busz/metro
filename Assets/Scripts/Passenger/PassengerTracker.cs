using TMPro;
using UnityEngine;

public class PassengerTracker : MonoBehaviour {
    [SerializeField]
    private Passenger _passenger;

    [SerializeField]
    private TextMeshProUGUI _passengerNameText;

    [SerializeField]
    private TextMeshProUGUI _passengerCurrentStopText;

    [SerializeField]
    private TextMeshProUGUI _passengerDestinationStopText;

    [SerializeField]
    private TextMeshProUGUI _passengerStrategyText;

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            Passenger passenger = RayCastForPassenger();
            Debug.Log(passenger);
            if (_passenger != null) {
                ClearPassenger();
            }
            if (passenger != null) {
                SelectPassenger(passenger);
            }
        }
    }

    private Passenger RayCastForPassenger() {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        return hit.transform != null ? hit.transform.gameObject.GetComponent<Passenger>() : null;
    }
    private void SelectPassenger(Passenger passenger) {
        _passenger = passenger;
        BindPassengerEvents();
        HighlightPassenger();
        _passengerNameText.text = passenger.name;
        _passengerCurrentStopText.text = passenger.GetCurrentStop().Name;
        _passengerDestinationStopText.text = passenger.GetDestinationStop().Name;
        _passengerStrategyText.text = passenger.GetSitStrategy().GetType().Name;
    }

    private void ClearPassenger() {
        UnbindPassengerEvents();
        UnhighlightPassenger();
        _passenger = null;
        _passengerNameText.text = "";
        _passengerCurrentStopText.text = "";
        _passengerDestinationStopText.text = "";
        _passengerStrategyText.text = "";
    }

    private void HighlightPassenger() {
        _passenger.SpriteRenderer.color = Color.cyan;
    }

    private void UnhighlightPassenger() {
        _passenger.SpriteRenderer.color = Color.magenta;
    }

    private void BindPassengerEvents() {
        _passenger.currentStopChangedEvent += OnCurrentStopChanged;
        _passenger.destinationChangedEvent += OnDestinationChanged;
    }

    private void UnbindPassengerEvents() {
        _passenger.currentStopChangedEvent -= OnCurrentStopChanged;
        _passenger.destinationChangedEvent -= OnDestinationChanged;
    }
    private void OnCurrentStopChanged() {
        _passengerCurrentStopText.text = _passenger.GetCurrentStop().Name;
    }

    private void OnDestinationChanged() {
        _passengerDestinationStopText.text = _passenger.GetDestinationStop().Name;
    }
}
