using System.Collections.Generic;
using UnityEngine;

public delegate void DestinationChangedDelegete();
public delegate void CurrentStopChangedDelegate();

public class Passenger : MonoBehaviour {
    public DestinationChangedDelegete destinationChangedEvent;
    public CurrentStopChangedDelegate currentStopChangedEvent;

    [SerializeField]
    private MetroStop _destination;

    [SerializeField]
    private MetroStop _currentStop;
    private MetroStop _nextStop;

    [SerializeField]
    private MetroConnectionSchedule _metroConnectionSchedule;

    [SerializeField]
    private LookForSitStrategy _takeSitStrategy;

    private Metro _metro;
    private MetroPath _metroPath;
    private IEnumerator<MetroStop> _metroPathEnumerator;
    private Sit _takenSit;
    public SpriteRenderer SpriteRenderer { get; private set; }

    void Start() {
        _metroPath = gameObject.AddComponent<MetroPath>();
        _currentStop._metroArrived += OnMetroArrived;
        MetroStop[] path = _metroConnectionSchedule.GetPath(_currentStop, _destination);
        SetUpPath(path);
        SpriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    private void SetUpPath(MetroStop[] path) {
        _metroPath.SetPath(path);
        _metroPathEnumerator = _metroPath.GetEnumerator();
        _metroPathEnumerator.MoveNext();
        _nextStop = _metroPathEnumerator.MoveNext() ? _metroPathEnumerator.Current : null;
    }

    private void SetUpNextDestination() {
        _destination = _metroConnectionSchedule.GetRandomStop();
        while (_destination == _currentStop) {
            _destination = _metroConnectionSchedule.GetRandomStop();
        }
        MetroStop[] path = _metroConnectionSchedule.GetPath(_currentStop, _destination);
        SetUpPath(path);
        destinationChangedEvent?.Invoke();
    }

    private void OnMetroArrived(Metro metro) {
        if (IsNextOnPassengerPath(metro.GetNextStop())) {
            metro.RegisterToEnter(this);
        }
    }

    private void OnNextStopReached(MetroStop stop) {
        _currentStop = stop;
        currentStopChangedEvent?.Invoke();
        _metroPathEnumerator.MoveNext();
        _nextStop = _metroPathEnumerator.Current;
        if (stop == _destination || !IsNextOnPassengerPath(_metro.GetNextStop())) {
            _metro.RegisterToGetOff(this);
            Debug.Log("Fuck this shit I'am out");
        }
    }

    public void AttemptEnter(Metro metro) {
        SitManager sitManager = metro.SitManager;
        Sit[] availableSits = _takeSitStrategy.LookForSit(sitManager);
        if (availableSits.Length > 0) {
            Enter(metro);
            TakeSit(GetRandomSit(availableSits));
        }
    }

    private Sit GetRandomSit(Sit[] sits) {
        return sits[Random.Range(0, sits.Length)];
    }

    private void Enter(Metro metro) {
        _metro = metro;
        _currentStop._metroArrived -= OnMetroArrived;
        _metro.MetroPathFollower.OnNextStopReachedEvent += OnNextStopReached;
    }
    public void Enter(MetroStop stop) {
        _currentStop._metroArrived += OnMetroArrived;
        stop.Enter(this);
    }
    public void GetOff() {
        _metro.MetroPathFollower.OnNextStopReachedEvent -= OnNextStopReached;
        _metro = null;
        transform.parent = null;
        _takenSit.SetPassenger(null);
        Enter(_currentStop);
        if (_currentStop == _destination) {
            SetUpNextDestination();
        }
    }

    private void TakeSit(Sit sit) {
        _takenSit = sit;
        _takenSit.SetPassenger(this);
        transform.SetParent(_takenSit.transform, true);
        transform.localPosition = Vector3.zero;
    }

    private bool IsNextOnPassengerPath(MetroStop stop) {
        return _nextStop == stop;
    }

    public MetroStop GetCurrentStop() => _currentStop;
    public MetroStop GetDestinationStop() => _destination;
    public LookForSitStrategy GetSitStrategy() => _takeSitStrategy;
}
