using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "WindowEnjoyerLookForSitStrategy", menuName = "LookForSitStrategy/WindowEnjoyer", order = 1)]
public class WindowEnjoyerLookForSitStrategy : LookForSitStrategy {
    public override Sit[] LookForSit(SitManager sitManager) {
        return sitManager.Where(sit => sit.IsFree() && sit.IsNearWindow()).ToArray();
    }
}
