using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "PoliteLookForSitStrategy", menuName = "LookForSitStrategy/Polite", order = 1)]
public class PoliteLookForSitStrategy : LookForSitStrategy {
    public override Sit[] LookForSit(SitManager sitManager) {
        return sitManager.Where(sit => sit.IsFree() && sit.GetDedicatedFor() != Sit.SitDedicationType.DISABLED).ToArray();
    }
}
