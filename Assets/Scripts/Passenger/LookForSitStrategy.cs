using UnityEngine;

public abstract class LookForSitStrategy : ScriptableObject {
    public abstract Sit[] LookForSit(SitManager sitManager);
}
