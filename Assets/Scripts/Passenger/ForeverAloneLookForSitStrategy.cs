using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "ForeverAloneLookForSitStrategy", menuName = "LookForSitStrategy/ForeverAlone", order = 1)]
public class ForeverAloneLookForSitStrategy : LookForSitStrategy {

    [SerializeField]
    private float _socialPhobiaRadius = 0.2f;

    public override Sit[] LookForSit(SitManager sitManager) {
        return sitManager.Where(sit => sit.IsFree() && checkIfAloneEnough(sitManager, sit)).ToArray();
    }

    private bool checkIfAloneEnough(SitManager sitManager, Sit sitCandidate) {
        foreach (Sit sit in sitManager) {
            bool isInCheckRange = Vector3.Distance(sitCandidate.transform.position, sit.transform.position) < _socialPhobiaRadius;
            if (sitCandidate != sit && isInCheckRange && !sit.IsFree())
                return false;
        }
        return true;
    }

}
