using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "DoNotGiveAFuckLookForSitStrategy", menuName = "LookForSitStrategy/DoNotGiveAFuck", order = 1)]
public class DoNotGiveAFuckLookForSitStrategy : LookForSitStrategy {
    public override Sit[] LookForSit(SitManager sitManager) {
        return sitManager.Where(sit => sit.IsFree()).ToArray();
    }
}
